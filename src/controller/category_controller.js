import { Router } from "express";
import { CategoryRepo } from "../repository/CategoryRepo";
import { validateCat } from "../Validator/validator";

export const categoryRoute = Router();

let catRepo = new CategoryRepo();

/**
 * find all data
 */
categoryRoute.get('/', async (req, res) => {
    try {
        let data = await catRepo.findAllCat();
        res.json(data);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});
/**
 * find data by id
 */
categoryRoute.get('/:id', async (req, res) => {
    try {
        let data = await catRepo.findById(req.params.id)
        if (data == null) {
            res.status(404).end()
            return
        }
        res.json(data);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
/**
 * send object as data to add in the table
 */
categoryRoute.post('/', async (req, res) => {
    try {
        if (validateCat(req.body) == false) {
            res.status(422).end()
            return false
        }
        await catRepo.addCat(req.body);
        res.status(201).json(req.body);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }

});
/**
 * update a row by his id
 */
categoryRoute.patch('/:id', async(req,res)=>{
    try {
        let data = await catRepo.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        let update = {...data, ...req.body};
        await catRepo.updateCat(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
/**
 * delete a row by his id
 */
categoryRoute.delete('/:id', async (req,res)=>{
    try {
        let data = await catRepo.deleteCat(req.params.id);
        if (data === true) {
            res.status(204).end()
            return
        } res.status(404).end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

