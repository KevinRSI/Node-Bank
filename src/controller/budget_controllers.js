import { Router } from "express";
import { BudgetRepo } from "../repository/BudgetRepo";
import { validateBudget } from "../Validator/validator";
let buRepo = new BudgetRepo();

export const budgetRoute = Router();
/**
 * find all data
 */
budgetRoute.get('/', async (req, res) => {
    try {
        let data = await buRepo.findAll()
        if (data === null) {
            res.status(404).end();
            return
        }
        res.json(data)
    } catch (error) {
        res.sendStatus(500).end();
        console.log(error)
    }
})
/**
 * find data by id
 */
budgetRoute.get('/:id', async (req, res) => {
    try {
        let data = await buRepo.findById(req.params.id)
        if (data == null) {
            res.status(404).end();
            return
        }
        res.json(data)
    } catch (error) {
        res.sendStatus(500).end();
        console.log(error);
    }

});
/**
 * send object as data to add in the table
 */
budgetRoute.post('/', async (req, res) => {
    try {
        if (validateBudget(req.body) == false) {
            res.status(422).end()
            return false
        }
        await buRepo.addBudget(req.body);
            res.status(201).json(req.body);

    } catch (error) {
        res.sendStatus(500).end();
        console.log(error);
    }
});
/**
 * update a row by his id
 */
budgetRoute.patch('/:id', async (req, res) => {
    try {
        let data = await buRepo.findById(req.params.id);
        if (!data) {
            res.sendStatus(404);
            return
        }
        let update = { ...data, ...req.body };
        await buRepo.UpdateBudget(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});
/**
 * delete a row by his id
 */
budgetRoute.delete('/:id', async (req, res) => {
    try {
        let data = await buRepo.delete(req.params.id);
        if (data === true) {
            res.status(204).end()
            return
        } res.status(404).end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})