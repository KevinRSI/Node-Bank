import { Router } from "express";
import { OperationsRepo } from "../repository/OperationsRepo";
import { validateOpe } from "../Validator/validator";

export const operationsRoute = Router();

let opRepo = new OperationsRepo();

/**
 * find data by user_id
 */
operationsRoute.get('/user/:id', async (req, res) => {
    try {
        let data = await opRepo.findAllByUserId(req.params.id, req.query.start, req.query.end);
        if (data == null) {
            res.status(404).end();
            return
        }
        res.json(data);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});

/**
 * find data by id
 */
operationsRoute.get('/:id', async (req, res) => {
    try {
        let data = await opRepo.findByOpId(req.params.id);
        if (data == null) {
            res.status(404).end();
            return
        }
        res.json(data);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

/**
 * send object as data to add in the table
 */
operationsRoute.post('/', async (req, res) => {
    try {
        if (validateOpe(req.body) == false) {
            res.status(422).end()
            return false
        }
        await opRepo.addOp(req.body);
        res.status(201).json(req.body);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
/**
 * delete a row by his id
 */
operationsRoute.delete('/:id', async (req, res) => {

    try {
        let data = await opRepo.deleteOp(req.params.id);
        if (data === true) {
            res.status(204).end();
            return
        }
        res.status(404).end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
/**
 * update a row by his id
 */
operationsRoute.patch('/:id', async (req, res) => {
    try {
        let data = await opRepo.findByOpId(req.params.id);
        if (!data) {
            res.sendStatus(404).end();
            return
        }
        let update = { ...data, ...req.body };
        await opRepo.updateOp(update)
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})