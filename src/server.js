import express, { json } from "express";
import { budgetRoute } from "./controller/budget_controllers";
import { categoryRoute } from "./controller/category_controller";
import { operationsRoute } from "./controller/operations_controllers";
import cors from 'cors';
export const server = express();

server.use(express.json());

server.use(cors())


server.get('/', async (req,res)=>{
    res.json('it\'s alive');
})


server.use('/api/budget', budgetRoute);
server.use('/api/category', categoryRoute);
server.use('/api/operations', operationsRoute);


