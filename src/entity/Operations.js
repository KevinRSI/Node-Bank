export class Operations{
    op_id;
    amount;
    category_id;
    description;
    user_id;
    date;

    constructor(oid, amount, catId, desc, uid, date) {
        this.op_id = oid;
        this.amount = amount;
        this.category_id = catId;
        this.description = desc;
        this.user_id = uid;
        this.date = date;
    }
}