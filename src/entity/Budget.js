export class Budget {
    id;
    name;
    total;
    date_start;
    date_end;

    constructor(id, total, name, dateS, dateN) {
        this.id = id;
        this.name = name;
        this.total = total;
        this.date_start = dateS;
        this.date_end = dateN;
    }
}