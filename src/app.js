import 'dotenv-flow/config';
import { server } from "./server";
import { validate } from './Validator/validator';

let port = process.env.PORT || 3000;



server.listen(port, ()=>{
    console.log('connected to localhost on port '+port);
})
