import Joi from "joi";

export function validateBudget(value) {
    let storeV = value;
    const schema = Joi.object({
        name: Joi.string()
            .alphanum()
            .min(1)
            .max(30)
            .required(),

        total: Joi.number()
            .required(),

        date_start: Joi.date()
            .required(),

        date_end: Joi.date()
            .required(),

    })
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, data } = schema.validate(value, options);

    if (error) {
        return console.log(error), false;
    } else {
        return value
    }
};

export function validateOpe(value) {
    let storeV = value;
    const schema = Joi.object({
        amount: Joi.number()
            .required(),

        category_id: Joi.number()
            .required(),

        description: Joi.string()
            .min(0)
            .max(256)
            .regex(/[A-Za-z\d\s]/),

        user_id: Joi.number()
            .required(),
        
        date: Joi.date()
            .required()

    })
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, data } = schema.validate(value, options);

    if (error) {
        return console.log(error), false;
    } else {
        return value
    }
};

export function validateCat(value) {
    let storeV = value;
    const schema = Joi.object({
        name: Joi.string()
            .alphanum()
            .min(1)
            .max(40)
            .required()

    })
    const options = {
        abortEarly: false, // include all errors
        allowUnknown: true, // ignore unknown props
        stripUnknown: true // remove unknown props
    };

    const { error, data } = schema.validate(value, options);

    if (error) {
        return console.log(error), false;
    } else {
        return value
    }
};