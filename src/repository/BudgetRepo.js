import { Budget } from "../entity/Budget";
import { connection } from "./connection";


export class BudgetRepo {


    async addBudget(item) {
        let [data] = await connection.execute('INSERT INTO budget(total,name,date_start,date_end) VALUES(?,?,?,?)', [item.total, item.name, item.date_start, item.date_end]);
        return item.id = data.insertId
    }

    async UpdateBudget(item) {
        await connection.execute('UPDATE budget SET total=?, name=?, date_start=?, date_end=? WHERE user_id=?', [item.total, item.name, item.date_start, item.date_end, item.id]);

    }

    async findById(id) {
        let [rows] = await connection.execute('SELECT * FROM budget WHERE user_id=?', [id]);
        if (rows.length != 0) {
            let budget = new Budget(rows[0].user_id, rows[0].total, rows[0].name, rows[0].date_start, rows[0].date_end);
            return budget;
        } return null;
    }

    async findAll() {
        let [rows] = await connection.execute('SELECT * FROM budget');
        let budget = []
        if (rows.length != 0) {
            for (const row of rows) {
                let instance = new Budget(row.user_id, row.total, row.name, row.date_start, row.date_end);
                budget.push(instance);
            }
            return budget
        } return null;
    }

    async delete(id) {
        let [rows] = await connection.execute('DELETE FROM budget WHERE user_id=?', [id]);
        if (rows.affectedRows === 1) {
            return true;
        } return false;
    }

}