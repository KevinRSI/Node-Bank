import { Operations } from "../entity/Operations";
import { connection } from "./connection";

export class OperationsRepo {
    async findAllByUserId(id, date_start, date_end) {
        let [rows] = await connection.execute('SELECT * FROM operations WHERE user_id=? AND date BETWEEN ? AND ?', [id, date_start, date_end]);
        let operation = [];
        for (const row of rows) {
            let instance = new Operations(row.op_id, row.amount, row.category_id, row.description, row.user_id, row.date);
            operation.push(instance);
        }
        if (operation.length != 0) {
            return operation;
        }
        return null
    }

    async findByOpId(id) {
        let [rows] = await connection.execute('SELECT * FROM operations WHERE op_id=?', [id]);
        if (rows.length != 0) {
            let operation = new Operations(rows[0].op_id, rows[0].amount, rows[0].category_id, rows[0].description, rows[0].user_id, rows[0].date);
            return operation;
        }
        return null

    }

    async addOp(item) {
        let [data] = await connection.execute('INSERT INTO operations(amount,category_id,description,user_id,date) VALUES(?,?,?,?,?)', [item.amount, item.category_id, item.description, item.user_id, item.date]);
        return item.op_id = data.insertId;
    }

    async updateOp(item) {
        await connection.execute('UPDATE operations SET amount=?, category_id=?, description=?, user_id=?, date=? WHERE op_id=?', [item.amount, item.category_id, item.description, item.user_id, item.date, item.op_id])
    }

    async deleteOp(opId) {
        let [data] = await connection.execute('DELETE FROM operations WHERE op_id=?', [opId]);
        if (data.affectedRows === 1) {
            return true;
        } return false;
    }
}