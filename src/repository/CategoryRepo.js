import { Category } from "../entity/Category";
import { connection } from "./connection";

export class CategoryRepo {
    async addCat(item) {
        let [data] = await connection.execute('INSERT INTO category (name) VALUES(?)', [item.name]);
        return item.id = data.insertId
    }

    async findAllCat() {
        let [rows] = await connection.execute('SELECT * FROM category');
        let category = [];
        for (const row of rows) {
            let instance = new Category(row.id, row.name);
            category.push(instance);
        }
        if (category.length != 0) {
            return category;
        }
        return null
    }

    async findById(id) {
        let [rows] = await connection.execute('SELECT * FROM category WHERE id=?', [id]);
        if (rows.length != 0) {
            let category = new Category(rows[0].id, rows[0].name);
            return category;
        } return null;
    }

    async updateCat(item) {
        let data = await connection.execute('UPDATE category SET name=? WHERE id=?', [item.name, item.id])
        return data
    }

    async deleteCat(id) {
        let [data] = await connection.execute('DELETE FROM category WHERE id=?', [id]);
        if (data.affectedRows === 1) {
            return true
        }
        return false
    }
}