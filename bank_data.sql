DROP TABLE IF EXISTS operations;
DROP TABLE IF EXISTS budget;
DROP TABLE IF EXISTS category;

CREATE TABLE `budget` (
  `user_id` int(11) AUTO_INCREMENT  NOT NULL COMMENT 'primary key',
  `name` VARCHAR(20) DEFAULT NULL,
  `total` float DEFAULT NULL COMMENT 'total amount of money',
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;


CREATE TABLE `operations` (
  `op_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `amount` float NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`op_id`),
  KEY `fk_cat_id` (`category_id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_cat_id` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `budget` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO budget(user_id,total,date_start,date_end) VALUES(1,700.5,'2021-06-01','2021-06-30');

INSERT INTO category(id,name) VALUES(1,'Dépenses journalières'),(2,'Loisirs '),(3,'Transports'),(4,'Habillement');
INSERT INTO operations(op_id,amount,category_id,description,user_id,date) VALUES(2,19.32,1,'uber eats',1,'2021-06-16'),(3,13.09,3,'TCL',1,'2021-06-17'),(4,59.99,2,'NieR Replicant',1,'2021-06-20');